import { Component, OnInit } from "@angular/core";
import { faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FormControl } from "@angular/forms";
import { MovieService } from "../services/movie.service";
import { Subscription } from "rxjs";
import { Movie } from "../models/movie";

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.scss"]
})
export class ModalComponent implements OnInit {
  faCheck = faCheck;
  faTimes = faTimes;

  display = "none";

  originalTitle = new FormControl();
  startYear = new FormControl();
  genres = new FormControl();

  movieSubscription: Subscription;
  private id: string;

  constructor(private movieService: MovieService) {}

  ngOnInit() {
    this.movieSubscription = this.movieService.modalSubject.subscribe(
      (movie: Movie) => {
        this.originalTitle.setValue(movie.originalTitle);
        this.startYear.setValue(movie.startYear);
        this.genres.setValue(movie.genres);
        this.id = movie.tconst;
        this.display = "block";
      }
    );
  }

  onModificationMovie() {
    this.movieService.changeMovie(
      this.id,
      this.originalTitle.value,
      this.startYear.value,
      this.genres.value,
      () => this.onClose()
    );
  }

  onClose() {
    this.display = "none";
  }
}
