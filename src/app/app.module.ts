import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { GlobalContainerComponent } from "./global-container/global-container.component";
import { SearchComponent } from "./search/search.component";
import { ResultsComponent } from "./results/results.component";
import { ModalComponent } from "./modal/modal.component";
import { HttpClientModule } from "@angular/common/http";
import { SortPipe } from "./services/sortBy.pipe";

@NgModule({
  declarations: [
    AppComponent,
    GlobalContainerComponent,
    SearchComponent,
    ResultsComponent,
    ModalComponent,
    SortPipe
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [SortPipe],
  bootstrap: [AppComponent]
})
export class AppModule {}
