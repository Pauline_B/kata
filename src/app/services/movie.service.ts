import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Subject } from "rxjs";
import { Movie } from "../models/movie";

@Injectable({
  providedIn: "root"
})
export class MovieService {
  moviesSubject = new Subject<Movie[]>();
  modalSubject = new Subject<Movie>();

  private movies: Movie[];
  private resultMovies: Movie[];
  private movie: Movie;
  private resultMovieIndex: number;

  constructor(private http: HttpClient) {}

  emitMoviesSubject() {
    this.moviesSubject.next(this.resultMovies.slice());
  }

  emitModalSubject() {
    this.modalSubject.next(this.movie);
  }

  initMovies() {
    this.http.get("../assets/data/movie.json").subscribe((data: Movie[]) => {
      this.movies = data;
      this.resultMovies = this.movies;
      this.emitMoviesSubject();
    });
  }

  findByTitle(title: string) {
    console.log(title);
    this.resultMovies = this.movies.filter(
      movie =>
        movie.originalTitle.toLowerCase().includes(title.toLowerCase()) ||
        movie.primaryTitle.toLowerCase().includes(title.toLowerCase())
    );
    this.emitMoviesSubject();
  }

  selectedMovie(movie: Movie, index: number) {
    this.movie = movie;
    this.resultMovieIndex = index;
    this.emitModalSubject();
  }

  changeMovie(
    id: string,
    title: string,
    startYear: number,
    genres: string,
    callback
  ) {
    const index = this.movies.findIndex(movie => movie.tconst === id);
    this.movies[index].originalTitle = title;
    this.movies[index].startYear = startYear;
    this.movies[index].genres = genres;
    this.resultMovies[this.resultMovieIndex] = this.movies[index];
    this.emitMoviesSubject();
    callback();
  }
}
