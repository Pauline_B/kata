import { Pipe, PipeTransform } from "@angular/core";
import { Movie } from "../models/movie";

@Pipe({
  name: "sort"
})
export class SortPipe implements PipeTransform {
  transform(movies: Movie[]): Movie[] {
    return movies.sort((a, b) => {
      return b.startYear - a.startYear;
    });
  }
}
