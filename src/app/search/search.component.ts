import { Component, OnInit } from "@angular/core";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FormControl } from "@angular/forms";
import { MovieService } from "../services/movie.service";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"]
})
export class SearchComponent implements OnInit {
  faSearch = faSearch;

  research = new FormControl();

  constructor(private movieService: MovieService) {}

  ngOnInit() {}

  onSubmit() {
    this.movieService.findByTitle(this.research.value);
  }
}
