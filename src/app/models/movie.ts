export class Movie {
  tconst: string;
  titleType: string;
  primaryTitle: string;
  originalTitle: string;
  isAdult: number;
  startYear: number;
  endYear: string;
  runtimeMinutes: number;
  genres: string;
}
