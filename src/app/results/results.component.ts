import { Component, OnInit } from "@angular/core";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { MovieService } from "../services/movie.service";
import { Movie } from "../models/movie";
import { Subscription } from "rxjs";

@Component({
  selector: "app-results",
  templateUrl: "./results.component.html",
  styleUrls: ["./results.component.scss"]
})
export class ResultsComponent implements OnInit {
  faPencilAlt = faPencilAlt;
  movies = [];
  moviesSubscription: Subscription;

  constructor(private movieService: MovieService) {}

  ngOnInit() {
    this.moviesSubscription = this.movieService.moviesSubject.subscribe(
      (movies: Movie[]) => {
        this.movies = movies;
      }
    );
    this.movieService.initMovies();
  }

  openModal(movie: Movie, index: number) {
    this.movieService.selectedMovie(movie, index);
  }
}
