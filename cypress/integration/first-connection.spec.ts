describe("Testing the list showed at connexion ", () => {
  beforeEach(() => {
    cy.server();
  });

  it("should correspond to the list send by the server", () => {
    cy.route("GET", /movie\.json/, "fixture:movies.json").as("getMovies");
    cy.visit("http://localhost:4200/");
    cy.get("#results-movies").as("movies");
    cy.wait("@getMovies");
    cy.get("@movies").contains("Carmencita");
  });
});
