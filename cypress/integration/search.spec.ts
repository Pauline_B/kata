describe("Testing the search part of the application ", () => {
  beforeEach(() => {
    cy.server();
    cy.visit("http://localhost:4200/");
  });

  it("should show the search input", () => {
    cy.get("#search-input");
  });

  it("should show the corresponding films in the result table", () => {
    cy.get("#search-input").type("car");
    cy.get("#search-button").click();
    cy.get("#results-title").contains("car");
  });
});
