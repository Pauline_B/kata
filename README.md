# GetStarted KATA : 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.4.

## Install App

To install all packages, you need to run the command : `npm install`

## Development server

To run the project locally, you need to run the command : `ng serve`. It will send you to the url `http://localhost:4200/`

## Running functionnal tests

To run the Cypress tests, you need to run the command : `npm run test`. 
It will open a new Cypress window, and let you choose beetwen tests you can run : first-connection and search.
You just have to click on the test you want to run

